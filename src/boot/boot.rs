// SPDX-License-Identifier: BSD 3-Clause
// Copyright (c) 2024 thinkerhui <thinkerhui@qq.com>
// Copyright (c) 2024 McDj26 <2645370670@qq.com>
// Copyright (c) 2024 qiyf6 <1076753979@qq.com>
use core::arch::global_asm;
global_asm!(
    include_str!("boot.s"),
    CONST_CORE_ID_MASK = const 0b11
);

#[no_mangle]
pub unsafe fn _start_rust() -> ! {
    crate::kernel_init()
}

// 指定了静态变量 BOOT_CORE_ID 的链接节（link section）为 .text._start_arguments。
// 这意味着编译器会将 BOOT_CORE_ID 放置在名为 .text._start_arguments 的节中。
#[no_mangle]
#[link_section = ".text._start_arguments"]
pub static BOOT_CORE_ID: u64 = 0;
