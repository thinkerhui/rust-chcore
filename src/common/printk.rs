// extern crate alloc;
use crate::common::uart::uart_send;

struct UartOutput;

impl core::fmt::Write for UartOutput {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for c in s.chars() {
            uart_send(c as u32);
        }
        Ok(())
    }
}

#[doc(hidden)]
pub fn printk(args: core::fmt::Arguments) {
    use core::fmt::Write;
    let mut output = UartOutput {};
    output.write_fmt(args).unwrap();
}

/// Prints without a newline.
#[macro_export]
macro_rules! uart_print {
    ($($arg:tt)*) => ($crate::common::printk::printk(format_args!($($arg)*)));
}

/// Prints with a newline.
#[macro_export]
macro_rules! uart_println {
    () => ($crate::printk!("\n"));
    ($($arg:tt)*) => ({
        $crate::common::printk::printk(format_args!($($arg)*));
    })
}
