// Filename: kprint.rs

/*
 * Copyright (c) 2020 Institute of Parallel And Distributed Systems (IPADS), Shanghai Jiao Tong University (SJTU)
 * OS-Lab-2020 (i.e., ChCore) is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *   http://license.coscl.org.cn/MulanPSL
 *   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 *   PURPOSE.
 *   See the Mulan PSL v1 for more details.
 */

pub const WARNING: usize = 0;
pub const INFO: usize = 1;
pub const DEBUG: usize = 2;
// extern crate alloc;

#[cfg(feature = "log-warning")]
#[macro_export]
macro_rules! kwarn {
    () => ($crate::uart_print!("[WARN] "));
    ($($arg:tt)*) => ($crate::common::printk::printk(format_args!($($arg)*)));
}

#[cfg(feature = "log-info")]
#[macro_export]
macro_rules! kinfo {
    () => ($crate::uart_print!("[INFO] "));
    ($($arg:tt)*) => ($crate::common::printk::printk(format_args!($($arg)*)));
}

#[cfg(feature = "log-debug")]
#[macro_export]
macro_rules! kdebug {
    () => ($crate::uart_print!("[DEBUG] "));
    ($($arg:tt)*) => ($crate::common::printk::printk(format_args!($($arg)*)));
 }

// If LOG_LEVEL is less than the log level, define empty macros
#[cfg(not(feature = "log-warning"))]
#[macro_export]
macro_rules! kwarn {
    ($($arg:tt)*) => {{}};
}

#[cfg(not(feature = "log-info"))]
#[macro_export]
macro_rules! kinfo {
    ($($arg:tt)*) => {{}};
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_kwarn_macro() {
        // Set LOG_LEVEL to WARNING
        let _log_output = kwarn!("This is a warning");
        // Add assertions based on your expectations
    }

    #[test]
    fn test_kinfo_macro() {
        // Set LOG_LEVEL to INFO
        let _log_output = kinfo!("This is an info message");
        // Add assertions based on your expectations
    }

    #[test]
    fn test_kdebug_macro() {
        // Set LOG_LEVEL to DEBUG
        let _log_output = kdebug!("This is a debug message");
        // Add assertions based on your expectations
    }
}
