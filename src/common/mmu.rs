// Filename: mmu.rs

/*
 * Copyright (c) 2020 Institute of Parallel And Distributed Systems (IPADS), Shanghai Jiao Tong University (SJTU)
 * OS-Lab-2020 (i.e., ChCore) is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *   http://license.coscl.org.cn/MulanPSL
 *   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 *   PURPOSE.
 *   See the Mulan PSL v1 for more details.
 */

use crate::common::types::{paddr_t, vaddr_t};

pub type VmrProp = u64;

pub const VMR_READ: VmrProp = 1 << 0;
pub const VMR_WRITE: VmrProp = 1 << 1;
pub const VMR_EXEC: VmrProp = 1 << 2;
pub const KERNEL_PT: VmrProp = 1 << 3;

// 以下地址转换仅用于内核，内核依照该方法（立即映射）在mmu启动前为自己填写页表

/// Convert physical address to virtual address (in kernal)
pub fn phys_to_virt(x: paddr_t) -> vaddr_t {
    (x + crate::common::vars::KBASE) as vaddr_t
}

/// Convert virtual address to physical address (in kernal)
pub fn virt_to_phys(x: vaddr_t) -> paddr_t {
    (x - crate::common::vars::KBASE) as paddr_t
}
