// Filename: types.rs
// Importing necessary libraries or modules
// Define types corresponding to C types
// According to `https://pubs.opengroup.org/onlinepubs/9699919799/`
pub type blkcnt_t = i64;
pub type blksize_t = i64;
pub type clock_t = u64;
pub type clockid_t = u64;
pub type dev_t = u64;
pub type fsblkcnt_t = u64;
pub type fsfilcnt_t = u64;
pub type gid_t = u64;
pub type id_t = u64;
pub type ino_t = u64;
pub type key_t = u64;
pub type mode_t = u64;
pub type nlink_t = u64;
pub type off_t = i64;
pub type pid_t = i64;
pub type pthread_attr_t = u64;
pub type pthread_barrier_t = u64;
pub type pthread_barrierattr_t = u64;
pub type pthread_cond_t = u64;
pub type pthread_condattr_t = u64;
pub type pthread_key_t = u64;
pub type pthread_mutex_t = u64;
pub type pthread_mutexattr_t = u64;
pub type pthread_once_t = u64;
pub type pthread_rwlock_t = u64;
pub type pthread_rwlockattr_t = u64;
pub type pthread_spinlock_t = u64;
pub type pthread_t = u64;
pub type size_t = u64;
pub type ssize_t = i64;
pub type suseconds_t = i64;
pub type time_t = u64;
pub type timer_t = u64;
pub type trace_attr_t = u64;
pub type trace_event_id_t = u64;
pub type trace_event_set_t = u64;
pub type trace_id_t = u64;
pub type uid_t = u64;

// Additional types
//pub type bool = u8;
// const true: bool = 1;
// const false: bool = 0;
pub type paddr_t = u64;
pub type vaddr_t = u64;

pub type atomic_cnt = u64;

// Different platforms may have different cacheline size and features like prefetch
pub const CACHELINE_SZ: usize = 64;
pub fn pad_to_cache_line(n: usize) -> usize {
    // 这是一个用于将给定的大小n调整为缓存行大小（CACHELINE_SZ）的倍数的函数
    // 它返回的是要增加多少大小
    // 这样做的目的通常是为了使数据在存储器中对齐到缓存行的边界，以提高访问效率
    // 调用ROUND_UP宏
    crate::ROUND_UP!(n, CACHELINE_SZ) - n
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pad_to_cache_line(){
        assert_eq!(pad_to_cache_line(128), 0);
        assert_eq!(pad_to_cache_line(100), 28);
        assert_eq!(pad_to_cache_line(0),0);
    }
}