// Filename: machine.rs

/*
 * Copyright (c) 2020 Institute of Parallel And Distributed Systems (IPADS), Shanghai Jiao Tong University (SJTU)
 * OS-Lab-2020 (i.e., ChCore) is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *   http://license.coscl.org.cn/MulanPSL
 *   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 *   PURPOSE.
 *   See the Mulan PSL v1 for more details.
 */

use crate::common::vars::KBASE;

const PLAT_CPU_NUM: usize = 4;

// Timers interrupt control registers
const IMER_IRQCNTL_BASE: usize = (KBASE + 0x40000040) as usize;
const CORE0_TIMER_IRQCNTL: usize = IMER_IRQCNTL_BASE + 0x0;
const CORE1_TIMER_IRQCNTL: usize = IMER_IRQCNTL_BASE + 0x4;
const CORE2_TIMER_IRQCNTL: usize = IMER_IRQCNTL_BASE + 0x8;
const CORE3_TIMER_IRQCNTL: usize = IMER_IRQCNTL_BASE + 0xc;
const INT_SRC_TIMER3: usize = 0x008;

// IRQ & FIQ source registers
const IRQ_BASE: usize = (KBASE + 0x40000060) as usize;
const CORE0_IRQ: usize = IRQ_BASE + 0x0;
const CORE1_IRQ: usize = IRQ_BASE + 0x4;
const CORE2_IRQ: usize = IRQ_BASE + 0x8;
const CORE3_IRQ: usize = IRQ_BASE + 0xc;
