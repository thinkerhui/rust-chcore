// use std::arch::global_asm;

// Filename: tools.rs
// Assembly counterpart to this file.
// global_asm!(include_str!("tools.S"));
// use std::arch::asm;
// // addr:u64会报错
// pub fn put32(addr:u64 ,data: u32){
//     let addr_i32 = addr as i32;
//     unsafe {
//         asm!(
//             "str {data},[{addr}]",
//             addr = in(reg) addr_i32,
//             data = in(reg) data
//         );
//     }
// }
// pub fn get32(addr:u64)->u32{
//     let result:u32;
//     let addr_i32 = addr as i32;
//     unsafe {
//         asm!(
//             "ldr {result},[{addr}]",
//             result = out(reg) result,
//             addr = in(reg) addr_i32
//         );
//     }
//     result
// }

// Define the equivalent Rust functions
#[link(name = "tools")]
extern "C" {
    pub fn put32(addr: u64, data: u32);
    pub fn get32(addr: u64) -> u32;
    pub fn delay(time: u32);
}