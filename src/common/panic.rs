use aarch64_cpu::asm;
use core::panic::PanicInfo;
// simple panic handler
// 这里rust-analyzer会默认报错，所以要配置一下，见.vscode/settings.json
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {
        asm::wfe()
    }
}
