// SPDX-License-Identifier: MIT OR Apache-2.0
//
// Copyright (c) 2018-2023 Andre Richter <andre.o.richter@gmail.com>

// SPDX-License-Identifier: MulanPSL-2.0
//
// Copyright (c) 2024-2025 thinkerhui <1142200122@qq.com>

use core::fmt;
use core::result::Result::Ok;
/// A mystical, magical device for generating QEMU output out of the void.
struct QEMUOutput;
// 这是一个神奇的qemu输出，只要把字符写进这个内存缓冲区，
// 不管实际的设备，qemu就能打印出来
impl fmt::Write for QEMUOutput {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for c in s.chars() {
            unsafe {
                core::ptr::write_volatile(0x3F20_1000 as *mut u8, c as u8);
            }
        }
        Ok(())
    }
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    let mut output = QEMUOutput {};
    output.write_fmt(args).unwrap();
}

/// Prints without a newline.
#[macro_export]
macro_rules! qemu_print {
    ($($arg:tt)*) => ($crate::_print(format_args!($($arg)*)));
}

/// Prints with a newline.
#[macro_export]
macro_rules! qemu_println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ({
        $crate::common::console::_print(format_args_nl!($($arg)*));
    })
}
