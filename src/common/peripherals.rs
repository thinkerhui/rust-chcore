// Filename: peripherals.rs

/*
   MIT License

   Copyright (c) 2018 Sergey Matyukevich

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

use crate::common::vars::KBASE;

/* This peripheral mapped offset is specific to BCM2837 */
const PHYSADDR_OFFSET: u64 = KBASE + 0x3F000000;

/* BCM2835 and BCM2837 define the same offsets */
const GPFSEL1: u64 = PHYSADDR_OFFSET + 0x00200004;
const GPSET0: u64 = PHYSADDR_OFFSET + 0x0020001C;
const GPCLR0: u64 = PHYSADDR_OFFSET + 0x00200028;
const GPPUD: u64 = PHYSADDR_OFFSET + 0x00200094;
const GPPUDCLK0: u64 = PHYSADDR_OFFSET + 0x00200098;

const AUX_ENABLES: u64 = PHYSADDR_OFFSET + 0x00215004;
const AUX_MU_IO_REG: u64 = PHYSADDR_OFFSET + 0x00215040;
const AUX_MU_IER_REG: u64 = PHYSADDR_OFFSET + 0x00215044;
const AUX_MU_IIR_REG: u64 = PHYSADDR_OFFSET + 0x00215048;
const AUX_MU_LCR_REG: u64 = PHYSADDR_OFFSET + 0x0021504C;
const AUX_MU_MCR_REG: u64 = PHYSADDR_OFFSET + 0x00215050;
const AUX_MU_LSR_REG: u64 = PHYSADDR_OFFSET + 0x00215054;
const AUX_MU_MSR_REG: u64 = PHYSADDR_OFFSET + 0x00215058;
const AUX_MU_SCRATCH: u64 = PHYSADDR_OFFSET + 0x0021505C;
const AUX_MU_CNTL_REG: u64 = PHYSADDR_OFFSET + 0x00215060;
const AUX_MU_STAT_REG: u64 = PHYSADDR_OFFSET + 0x00215064;
const AUX_MU_BAUD_REG: u64 = PHYSADDR_OFFSET + 0x00215068;
