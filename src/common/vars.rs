// Filename: vars.rs

// Leave 8K space to kernel stack
pub const KERNEL_STACK_SIZE: usize = 8192;
pub const IDLE_STACK_SIZE: usize = 8192;
pub const STACK_ALIGNMENT: usize = 16;

// Can be different in different architectures
pub const KBASE: u64 = 0xffffff0000000000;
