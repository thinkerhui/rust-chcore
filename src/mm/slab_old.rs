// use crate::mm::{buddy, buddy::BUDDY_PAGE_SIZE, mm::global_mem};
// use crate::{kdebug, kwarn, BUG_ON};
// use core::ptr;
// extern crate alloc;
// use alloc::boxed::Box;
// const SLAB_INIT_SIZE: usize = 2 * 1024 * 1024; // 2M
// const SLAB_MIN_ORDER: usize = 5;
// const SLAB_MAX_ORDER: usize = 11;
// // 使用数组表示 slabs
// static mut slabs: [Option<Box<SlabHeader>>; SLAB_MAX_ORDER + 1] = Default::default();

// struct SlabHeader {
//     free_list_head: *mut u8,
//     next_slab: Option<Box<SlabHeader>>,
//     order: usize,
// }

// struct SlabSlotList {
//     next_free: *mut u8,
// }

// impl SlabHeader {
//     fn new(order: usize) -> Self {
//         SlabHeader {
//             free_list_head: ptr::null_mut(),
//             next_slab: None,
//             order,
//         }
//     }
// }

// pub fn init_slab() {
//     // 初始化一个包含不同大小的 Slab 的数组
//     // SlabHeader可以通过链表把对应大小的空闲内存块给串起来
//     // slab obj size: 32, 64, 128, 256, 512, 1024, 2048
//     for order in SLAB_MIN_ORDER..=SLAB_MAX_ORDER {
//         unsafe {
//             slabs[order] = Some(init_slab_cache(order, SLAB_INIT_SIZE));
//         }
//     }
//     kdebug!("mm: finish initing slab allocators\n");
// }

// fn size_to_order(size: usize) -> usize {
//     // 工具函数
//     // 将给定的内存大小（size）转换为 Slab 分配器中的order
//     let mut order = 0;
//     let mut tmp = size;

//     while tmp > 1 {
//         tmp >>= 1;
//         order += 1;
//     }
//     if size > (1 << order) {
//         order += 1;
//     }

//     order
// }

// fn order_to_size(order: usize) -> usize {
//     // 将给定的slab order转化为内存大小
//     1 << order
// }

// fn alloc_slab_memory(size: u64) -> *mut u8 {
//     // alloc_slab_memory主要是为slab分配内存(页面),以方便让slab对其进行管理
//     // 计算需要分配的大小对应的slab order
//     let order = size_to_order(size as usize / BUDDY_PAGE_SIZE as usize);
//     // 通过伙伴系统从全局内存global_mem中获取对应的一组连续的内存页面
//     let p_page = buddy::buddy_get_pages(unsafe { &mut global_mem }, order as u64);

//     // 如果p_page为空，说明获取分配内存失败
//     if p_page.is_none() {
//         kwarn!("failed to alloc_slab_memory: out of memory\n");
//         BUG_ON!(true);
//     }
//     // 计算页面对应的虚拟地址
//     let addr = buddy::page_to_virt(unsafe { &global_mem }, p_page.unwrap());

//     // 对齐检查，检查虚拟地址是否按期望的大小对齐
//     // BUG_ON!(check_alignment(addr as u64, SLAB_INIT_SIZE));

//     let page_num = order_to_size(order);
//     // 遍历分配到的每一页，为每一页设置对应的 slab ,指向 Slab 的(虚拟)起始地址
//     for i in 0..page_num {
//         let page_addr = (addr as usize + i * BUDDY_PAGE_SIZE as usize) as *mut u8;
//         let page = buddy::virt_to_page(unsafe { &global_mem }, page_addr as u64);
//         dbg!(page.borrow_mut().data.slab = addr as *mut u8);
//     }

//     addr
// }

// //
// fn _alloc_in_slab_nolock(mut slab_header: Box<SlabHeader>, order: usize) -> *mut u8 {
//     // 在不使用锁的情况下，在 Slab 分配器的一个 Slab 中分配内存块
//     // 获取该Slab的第一个空闲内存块指针
//     let first_slot = slab_header.free_list_head as *mut SlabSlotList;
//     if !first_slot.is_null() {
//         // 如果第一个Slab的没有空闲内存块，尝试在其他Slab中查找可用的空闲内存块
//         let next_slot = unsafe { (*first_slot).next_free };
//         slab_header.free_list_head = next_slot;
//         first_slot as *mut u8
//     } else {
//         let mut next_slab = slab_header.next_slab.as_mut();
//         while let Some(slab) = next_slab {
//             let first_slot = slab.free_list_head as *mut SlabSlotList;
//             if !first_slot.is_null() {
//                 let next_slot = unsafe { (*first_slot).next_free };
//                 slab.free_list_head = next_slot;
//                 return first_slot as *mut u8;
//             }
//             next_slab = slab.next_slab.as_mut();
//         }
//         // Alloc new slab
//         let mut new_slab = init_slab_cache(order, SLAB_INIT_SIZE);
//         new_slab.next_slab = Some(slab_header);

//         unsafe {
//             slabs[order] = Some(new_slab);
//         }
//         _alloc_in_slab_nolock(new_slab, order)
//     }
// }

// fn _alloc_in_slab(slab_header: SlabHeader, order: usize) -> *mut u8 {
//     // 注意：这里的SlabHeader的&mut和Box我并没有想清楚该用哪个
//     let boxed_slab_header = Box::new(slab_header);
//     _alloc_in_slab_nolock(boxed_slab_header, order)
// }

// fn init_slab_cache(order: usize, size: usize) -> Box<SlabHeader> {
//     let addr = alloc_slab_memory(size as u64);
//     let mut slab = Box::new(SlabHeader::new(order));

//     let obj_size = order_to_size(order);
//     let cnt = size / obj_size - 1;

//     let slot = (addr as usize + obj_size) as *mut SlabSlotList;
//     slab.free_list_head = slot as *mut u8;

//     let mut current_slot = slot;
//     for _ in 0..cnt - 1 {
//         let next_slot = (current_slot as usize + obj_size) as *mut SlabSlotList;
//         unsafe {
//             (*current_slot).next_free = next_slot as *mut u8;
//         }
//         current_slot = next_slot;
//     }

//     unsafe {
//         (*current_slot).next_free = ptr::null_mut();
//     }

//     slab
// }

// fn alloc_in_slab(size: usize) -> *mut u8 {
//     // Your allocation code here
//     ptr::null_mut()
// }

// fn free_in_slab(addr: *mut u8) {
//     let slot = addr as *mut SlabSlotList;
//     let page = buddy::virt_to_page(unsafe { &global_mem }, addr as u64);
//     // 这里应该有错误处理，page有可能并不能得到
//     // BUG_ON!();
//     unsafe {
//         // 强行解引用
//         // 注意：这里假设 slab 是正确对齐的，否则需要使用 read_unaligned
//         let mut slab: SlabHeader = ptr::read(page.borrow_mut().data.slab as *const _);
//         (*slot).next_free = slab.free_list_head;
//         slab.free_list_head = slot as *mut u8;
//     }
// }
