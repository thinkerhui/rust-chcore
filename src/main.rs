// SPDX-License-Identifier: MIT OR Apache-2.0
//
// Copyright (c) 2018-2023 Andre Richter <andre.o.richter@gmail.com>
// SPDX-License-Identifier: BSD 3-Clause
// Copyright (c) 2024 thinkerhui <thinkerhui@qq.com>
// Copyright (c) 2024 McDj26 <2645370670@qq.com>
// Copyright (c) 2024 qiyf6 <1076753979@qq.com>
//! 这个一个简单的操作系统启动示例。 这行注释不能去掉，!也不能去掉，否则编译时会检查到缺少文档。

#![no_main]
#![no_std]
#![feature(asm_const)]
#![feature(panic_info_message)]
#![feature(format_args_nl)]
// 下面两个特性允许采用非const函数来初始化const，可能有更好的做法
#![feature(const_trait_impl)]
#![feature(effects)]
mod boot;
mod common;
mod mm;
unsafe fn kernel_init() -> ! {
    qemu_println!("Hello from the rust-chcore!\n");
    mm::mm::mm_init();
    qemu_println!("memory init succeed!");
    panic!("Stopping here.")
}
