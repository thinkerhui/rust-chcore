use std::os::raw::c_void;

pub extern "C" fn el1_mmu_activate() {
    // TODO: Implement el1_mmu_activate function
}

pub extern "C" fn init_boot_pt() {
    // TODO: Implement init_boot_pt function
}

pub extern "C" fn start_kernel(boot_flag: *mut c_void) {
    // TODO: Implement start_kernel function
}

pub const _bss_start: *const c_void = 0;
pub const _bss_end: *const c_void = 0;

pub const PLAT_CPU_NUMBER: usize = 4;

#[macro_preamble]
macro_rules! ALIGN {
    ($($t:ty)*) => {
        $(
        #[attribute(align(1))]
        $(($t))
        )*
    };
}