# rust-chcore

#### 介绍
rust重构chcore。重构对象为上海交通大学 IPADS《现代操作系统：原理与实现》ChCore 课程实验 v1采用的ChCore https://gitee.com/ipads-lab/chcore-lab

chcore是一个用于教学的微型操作系统内核。目前rust-chcore只重构到chcore的lab2(内存管理)。所以目前rust-chcore实现的功能是比较简单的：启动内核并进行内存管理初始化。
原chcore是用C语言实现的。在重构过程中，由于rust本身的语言特性(特别是安全特性)，为了能顺利通过rustc编译，会加入chcore原来没有的一些东西。

#### 快速运行
1. 编译：make
2. 运行：qemu-system-aarch64 -M raspi3b -serial stdio -display none -kernel kernel8.img

## 项目结构

```
├── Cargo.lock
├── Cargo.toml
├── ChCore研究.md
├── LICENSE
├── Makefile
├── README.en.md
├── README.md
├── build.rs
├── chcore_lab2
├── common
├── docker
├── kernel.ld
├── kernel8.img
├── rust-toolchain.toml
├── src
```




### 来自其他项目的文件

我们在实现的过程中，主要参考了两个项目的代码:

一个就是我们的重构对象[chcore-lab: 上海交通大学 IPADS《现代操作系统：原理与实现》ChCore 课程实验 v1](https://gitee.com/ipads-lab/chcore-lab)，
一个是[rust-embedded/rust-raspberrypi-OS-tutorials: :books: Learn to write an embedded OS in Rust :crab: (github.com)](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials)

内核的编译和启动主要参考rust-raspberrypi-OS，其中docker、common和src的boot大部分直接来自这里，不过难免要稍作修改。

此外，为了方便进行调试，也参考了rust-raspberrypi-OS中的qemu魔法输出([03_hacky_hello_world](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials/tree/master/03_hacky_hello_world))，这样可以不管硬件情况直接输出的一个神奇接口，具体见src/common的console.rs和panic.rs等。

虽然重构的对象是chcore，由于跨语言的问题，很少代码是直接复制原chcore的（只有诸如汇编之类的复制过来了,主要在tools）。不过为了方便我们自己打开项目进行对比，所以把chcore直至lab2的部分放在了chcore_lab2文件夹里面。由于原chcore是挖空的，我们lab2的未完成部分代码也参考了一些资料，其中主要是[chcore - 标签 - 康宇PL - 博客园 (cnblogs.com)](https://www.cnblogs.com/kangyupl/tag/chcore/),当然还有chcore本身的实验讲解资料。

### 项目代码

**项目本身的主要代码在src文件夹**

```bash
.
├── boot
│   ├── boot.rs
│   ├── boot.s
│   └── start.S
├── boot.rs
├── common
│   ├── console.rs
│   ├── errno.rs
│   ├── kprint.rs
│   ├── libtools.a
│   ├── list.rs
│   ├── machine.rs
│   ├── macros.rs
│   ├── mmu.rs
│   ├── panic.rs
│   ├── peripherals.rs
│   ├── printk.rs
│   ├── registers.rs
│   ├── synchronization.rs
│   ├── tools.S
│   ├── tools.rs
│   ├── types.rs
│   ├── uart.rs
│   ├── util.rs
│   └── vars.rs
├── common.rs
├── main.rs
├── mm
│   ├── buddy.rs
│   ├── kmalloc.rs
│   ├── mm.rs
│   ├── page_table.rs
│   ├── slab.rs
│   └── slab_old.rs
└── mm.rs
```

有三个文件夹:boot,common,mm.

boot主要存放和内核启动先关的代码

common主要存放常量和工具性质的代码，比如各种硬件IO对应的内存地址，打印，错误处理，变量类型定义等。

mm主要存放内存管理的代码，包括页表、slab分配器和伙伴系统等。

在src层的文件有boot.rs，common.rs，mm.rs，main.rs,前三者主要用来组织rust项目结构，main.rs则是内核主体的入口，注意不是内核的入口，内核是从boot开始启动的。

**此外，还有一些重要的代码在根目录**，这些代码主要是用于维护项目的结构组织、配置环境和编译。其中有五个重要的文件:build.rs,Makefile，Cargo.toml，rust-toolchain.toml和kernel.ld

可以从这些文件中看到项目编译和运行的各种环境配置。

其中，可以通过kernel.ld来看到编译好的内核的文件内存布局是怎样安排的，或者说内核的各个模块是怎样组合成一个整体的。





