// SPDX-License-Identifier: BSD 3-Clause
// Copyright (c) 2024 thinkerhui <thinkerhui@qq.com>
// Copyright (c) 2024 McDj26 <2645370670@qq.com>
// Copyright (c) 2024 qiyf6 <1076753979@qq.com>
use std::{env, fs, process};

fn main() {
    // 添加tools库的文件位置
    println!("cargo:rustc-link-search=./src/common");

    let ld_script_path = match env::var("LD_SCRIPT_PATH") {
        Ok(var) => var,
        _ => process::exit(0),
    };

    let files = fs::read_dir(ld_script_path).unwrap();
    files
        .filter_map(Result::ok)
        .filter(|d| {
            if let Some(e) = d.path().extension() {
                e == "ld"
            } else {
                false
            }
        })
        .for_each(|f| println!("cargo:rerun-if-changed={}", f.path().display()));
}
