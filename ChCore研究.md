# ChCore研究

rust-chcore准备重构chcore v1的lab1和lab2.

我们首先肯定要对lab1和lab2本身有了充分的了解，然后才好下手开始项目的重构。

现在准备在ChatGPT等大模型和网上各种资料的辅助下，完成对ChCore的源代码的初步了解。

![image-20231112191747514](https://sse-market-source-1320172928.cos.ap-guangzhou.myqcloud.com/blog/image-20231112191747514.png)

切换到lab2分支。

## 环境配置

![image-20231112193639797](https://sse-market-source-1320172928.cos.ap-guangzhou.myqcloud.com/blog/image-20231112193639797.png)

[Windows 上的 Docker Desktop WSL 2 后端 |Docker 文档](https://docs.docker.com/desktop/wsl/)

在docker desktop中启用WSL集成。这样就可以在WSL2子系统中直接用docker了。

![image-20231112193705068](https://sse-market-source-1320172928.cos.ap-guangzhou.myqcloud.com/blog/image-20231112193705068.png)

开始拉取docker镜像。

![image-20231112193927628](https://sse-market-source-1320172928.cos.ap-guangzhou.myqcloud.com/blog/image-20231112193927628.png)![image-20231112195408885](https://sse-market-source-1320172928.cos.ap-guangzhou.myqcloud.com/blog/image-20231112195408885.png)

成功学会用gdb学会调试。![image-20231112195650823](https://sse-market-source-1320172928.cos.ap-guangzhou.myqcloud.com/blog/image-20231112195650823.png)

## 根目录各文件解释

### config.cmake

这段代码是用 CMake 语言编写的，用于定义三个**配置变量**：CHCORE_CROSS_COMPILE、CHCORE_PLAT 和 CHCORE_VERBOSE_BUILD。

1. chcore_config(CHCORE_CROSS_COMPILE STRING "" "Prefix for cross compiling toolchain")：定义一个名为 CHCORE_CROSS_COMPILE 的配置变量，类型为字符串，初始值为空字符串。这个变量**用于指定交叉编译工具链的前缀**。
2. chcore_config(CHCORE_PLAT STRING "" "Target hardware platform")：定义一个名为 CHCORE_PLAT 的配置变量，类型为字符串，初始值为空字符串。这个变量**用于指定目标硬件平台**。
3. chcore_config(CHCORE_VERBOSE_BUILD BOOL OFF "Generate verbose build log?")：定义一个名为 CHCORE_VERBOSE_BUILD 的配置变量，类型为布尔值，初始值为关。这个变量**用于指定是否生成详细的构建日志**。
4. chcore_config_include(kernel/config.cmake)：将名为 **kernel/config.cmake** 的文件包含到当前 **CMakeLists.txt** 文件中。这个文件可能包含了与当前项目相关的配置信息。

### CMakeLists.txt

# rust OS研究

同时，有c和rust在系统引导方面似乎有些不同，所以要找一下arm平台的rust系统教程。

https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials

# 实现路径

## 初步猜想

目前大概有这样的实现路径想法（不过很有可能搞不成），就是chcore可以分为两个部分，其中一个部分是引导内核启动的程序，另一部分就是chcore的内核。
在引导内核启动这一块，rust和c的区别可能比较大，而我们实际想要做的重构工作主要是面对内核的。
所以我的想法是在引导内核启动的初始化这一块就参考现有rust的，目前也找到了能够跑在raspberrypi的[rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials)。然后我们进一步的工作把chcore的内核重构成rust，并和这个rust os的初始化相适应（或者说是修改这个rust-raspberrypi-OS的内核)。

由于内核的代码量也不是很多，所以乐观估计工作量应该不大。